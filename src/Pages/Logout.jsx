import React from 'react'
import { Navigate } from 'react-router-dom'
import authStore from 'src/Context/Auth.mobx'

const Logout = () => {
  authStore.setUser(null)

  return <Navigate to="/" />
}

export default Logout
