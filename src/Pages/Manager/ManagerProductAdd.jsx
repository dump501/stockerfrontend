import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CForm,
  CFormInput,
  CFormLabel,
  CFormSelect,
  CCol,
} from '@coreui/react'
import { useForm } from 'react-hook-form';
import { toFormData } from 'src/Helper/Helper';
import { addProduct, addUser, getRayons } from 'src/Api/Api';
import { useNavigate } from 'react-router-dom';
import ManagerLayout from './ManagerLayout';
import SpanError from 'src/components/SpanError';
import SpinnerPage from 'src/components/SpinnerPage';


const ManagerProductAdd = () => {
  const [loading, setLoading] = useState(false)
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [rayons, setrayons] = useState(null)
  let navigate = useNavigate()

  const handleForm = async (data) => {

    if (Object.keys(errors).length === 0) {
      setLoading(true)
      let formData = toFormData(data)
      formData.append("attributes", "dd")
      let response = await addProduct(formData)
      response = await response.json()
      console.log(response);
      setTimeout(() => {
        navigate("/manager/product")
      }, 3000);
    } else {
      alert("Tous les champs doivent être rempli")
    }

  }
  useEffect(() => {
    setLoading(true)
    const fetchData = async () => {
      let response = await getRayons()
      response = await response.json()
      setrayons(response.data)
    }
    fetchData()
    setLoading(false)
  }, [])


  return (
    <ManagerLayout>
      {loading ? <SpinnerPage /> :<CCard className="mb-4">
        <CCardHeader>
          <strong>Ajouter un produit</strong>
        </CCardHeader>
        <CCardBody>
          <CForm className="row g-3" onSubmit={handleSubmit(handleForm)}>
            <CCol md={6}>
              <CFormLabel>Nom {errors.name && <SpanError />}</CFormLabel>
              <CFormInput type="text" {...register("name", { required: true })} />
            </CCol>
            <CCol md={6}>
              <CFormLabel>Prix unitaire {errors.unitPrice && <SpanError />}</CFormLabel>
              <CFormInput type="number" {...register("unitPrice", { required: true })} />
            </CCol>
            <CCol md={6}>
              <CFormLabel>Rayon {errors.rayon_id && <SpanError />}</CFormLabel>
              <CFormSelect  {...register("rayon_id", { required: true })} >

                <option>Selectionner le rayon</option>
                {rayons && rayons.map((rayon, i) => <option key={i} value={rayon.id}>{rayon.name}</option>)}
              </CFormSelect>
            </CCol>
            <CCol xs={12}>
              <CButton type="submit">Enregistrer</CButton>
            </CCol>
          </CForm>
        </CCardBody>
      </CCard>}
    </ManagerLayout>
  )
}

export default ManagerProductAdd
