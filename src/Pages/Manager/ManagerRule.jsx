import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CFormInput,
  CFormLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CTable,
  CTableHeaderCell,
  CTableDataCell,
  CTableHead,
  CTableBody,
  CTableRow,
  CButtonGroup,
  CFormTextarea,
  CFormSelect,
  CFormSwitch,
  CForm,
  CBadge
} from '@coreui/react'
import { toFormData } from 'src/Helper/Helper'
import { addRule, addUserRule, getRules, getUserRules } from 'src/Api/Api'
import ManagerLayout from './ManagerLayout'
import SpanError from 'src/components/SpanError'
import { useForm } from 'react-hook-form'
import { toJS } from 'mobx'
import authStore from 'src/Context/Auth.mobx'
import { useNavigate } from 'react-router-dom'
import SpinnerPage from 'src/components/SpinnerPage'

const ManagerRule = () => {
  const [loading, setLoading] = useState(false)
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [visible, setVisible] = useState(false)
  const [updateVisible, setupdateVisible] = useState(false)
  const [userRules, setuserRules] = useState(null)
  const [rules, setrules] = useState(null)
  const navigate = useNavigate()

  const handleRuleAdd = async (data) => {
    console.log(data);
    if (Object.keys(errors).length === 0) {
      setLoading(true)
      let formData = toFormData(data)
      formData.append("userId", toJS(authStore.user).id)
      let response = await addUserRule(formData)
      response = await response.json()
      console.log(response);
      setTimeout(() => {
        navigate("/manager/stock")
      }, 3000);
    } else {
      alert("Tous les champs doivent être rempli")
    }
  }

  const getFrequence = (frequence) => {
    switch (frequence) {
      case 1:
        return "Journalier"
      case 2:
        return "Hepdomadaire"
      case 3:
        return "Mensuel"

      default:
        break;
    }
  }

  useEffect(() => {
    setLoading(true)
    const fetchData = async () => {
      let rules = await getRules()
      rules = await rules.json()
      setrules(rules.data)

      let userRules = await getUserRules(toJS(authStore.user).id)
      userRules = await userRules.json()
      console.log("user rules", userRules);
      setuserRules(userRules.data)
    }
    fetchData()
    setLoading(false)
  }, [])
  return (
    <ManagerLayout>
      {loading ? <SpinnerPage /> :<><CCard>
        <CCardHeader><strong>Liste des règles de notifications</strong></CCardHeader>
        <CCardBody>
          <CButton onClick={() => setVisible(true)}>Ajouter une Règle</CButton>
          <div style={{ margin: "2rem 0" }}>
            <CTable>
              <CTableHead>
                <CTableRow>
                  <CTableHeaderCell scope="col">Id</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Règle</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Status</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Fréquence</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Valeur limite</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Actions</CTableHeaderCell>
                </CTableRow>
              </CTableHead>
              <CTableBody>
                {userRules && userRules.map((rule, i) => (<CTableRow key={i}>
                  <CTableDataCell>{i + 1}</CTableDataCell>
                  <CTableDataCell>{rule.rule.name}</CTableDataCell>
                  <CTableDataCell>{rule.is_enabled == 1 ? <CBadge color='success'>Activé</CBadge> : <CBadge color='danger'>Désactivé</CBadge>}</CTableDataCell>
                  <CTableDataCell>{getFrequence(rule.frequence)}</CTableDataCell>
                  <CTableDataCell>{rule.parameters}</CTableDataCell>
                  <CTableDataCell>
                    <CButton color='primary' onClick={(e)=>{setupdateVisible(true)}}>Editer</CButton>
                  </CTableDataCell>
                </CTableRow>))}
              </CTableBody>
            </CTable>
          </div>
          
        </CCardBody>
      </CCard><br />
      <CCard>
        <CCardHeader><strong>Description des règles</strong></CCardHeader>
        <CCardBody>
          <CTable>
            <CTableHead>
              <CTableRow>
                <CTableHeaderCell scope="col">Id</CTableHeaderCell>
                <CTableHeaderCell scope="col">Nom</CTableHeaderCell>
                <CTableHeaderCell scope="col">Description</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {rules && rules.map((rule, i) => (<CTableRow key={i}>
                <CTableDataCell>{i + 1}</CTableDataCell>
                <CTableDataCell>{rule.name}</CTableDataCell>
                <CTableDataCell>{rule.description}</CTableDataCell>
              </CTableRow>))}
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
      <CModal visible={visible} onClose={() => setVisible(false)}>
        <CModalHeader>
          <CModalTitle>Ajouter une règle</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm onSubmit={handleSubmit(handleRuleAdd)}>
            <CFormLabel>Selectioner la règle {errors.ruleId && <SpanError />}</CFormLabel>
            <CFormSelect {...register("ruleId", { required: true })}>
              <option>Selectionner la règle</option>
              {rules && rules.map((rule, i) => <option key={i} value={rule.id}>{rule.name}</option>)}
            </CFormSelect><br />
            <CFormLabel>status de la règle {errors.isEnabled && <SpanError />}</CFormLabel>
            <CFormSelect {...register("isEnabled", { required: true })}>
              <option>status de la règle</option>
              <option value={1}>Activé</option>
              <option value={0}>Désactivé</option>
            </CFormSelect><br />
            <CFormLabel>frequence de notification  {errors.frequence && <SpanError />}</CFormLabel>
            <CFormSelect {...register("frequence", { required: true })}>
              <option>fréquence de notification</option>
              <option value={1}>Journalier</option>
              <option value={2}>hepdomadaire</option>
              <option value={3}>mensuel</option>
            </CFormSelect><br />
            <CFormLabel>Valeur limite {errors.frequence && <SpanError />}</CFormLabel>
            <CFormInput
              type='number'
              placeholder='valeur limit'
              {...register("parameters", { required: true })} /><br />
            <CButton color="primary" type='submit'>Enregistrer</CButton>
            <CButton color="secondary" onClick={() => setVisible(false)}>
              Fermer
            </CButton>
          </CForm>
        </CModalBody>
        {/* <CModalFooter>
          <CButton color="primary" onClick={() => { handleRuleAdd() }}>Enregistrer</CButton>
          <CButton color="secondary" onClick={() => setVisible(false)}>
            Fermer
          </CButton>
        </CModalFooter> */}
      </CModal>
      <CModal visible={updateVisible} onClose={() => setupdateVisible(false)}>
        <CModalHeader>
          <CModalTitle>Modifier une règle</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm onSubmit={handleSubmit(handleRuleAdd)}>
            <CFormLabel>Selectioner la règle {errors.ruleId && <SpanError />}</CFormLabel>
            <CFormSelect {...register("ruleId", { required: true })}>
              <option>Selectionner la règle</option>
              {rules && rules.map((rule, i) => <option key={i} value={rule.id}>{rule.name}</option>)}
            </CFormSelect><br />
            <CFormLabel>status de la règle {errors.isEnabled && <SpanError />}</CFormLabel>
            <CFormSelect {...register("isEnabled", { required: true })}>
              <option>status de la règle</option>
              <option value={1}>Activé</option>
              <option value={0}>Désactivé</option>
            </CFormSelect><br />
            <CFormLabel>frequence de notification  {errors.frequence && <SpanError />}</CFormLabel>
            <CFormSelect {...register("frequence", { required: true })}>
              <option>fréquence de notification</option>
              <option value={1}>Journalier</option>
              <option value={2}>hepdomadaire</option>
              <option value={3}>mensuel</option>
            </CFormSelect><br />
            <CFormLabel>Valeur limite {errors.frequence && <SpanError />}</CFormLabel>
            <CFormInput
              type='number'
              placeholder='valeur limit'
              {...register("parameters", { required: true })} /><br />
            <CButton color="primary" type='submit'>Enregistrer</CButton>
            <CButton color="secondary" onClick={() => setupdateVisible(false)}>
              Fermer
            </CButton>
          </CForm>
        </CModalBody>
        {/* <CModalFooter>
          <CButton color="primary" onClick={() => { handleRuleAdd() }}>Enregistrer</CButton>
          <CButton color="secondary" onClick={() => setVisible(false)}>
            Fermer
          </CButton>
        </CModalFooter> */}
      </CModal></>}
    </ManagerLayout>
  )
}

export default ManagerRule
