import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CFormInput,
  CFormLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CTable,
  CTableHeaderCell,
  CTableDataCell,
  CTableHead,
  CTableBody,
  CTableRow,
  CButtonGroup,
  CRow,
  CFormSelect,
  CCol
} from '@coreui/react'
import { toFormData } from 'src/Helper/Helper'
import { addRayon, addUnit, deleteRayon, deleteUnit, getProducts, getRayons, getUnits } from 'src/Api/Api'
import ManagerLayout from './ManagerLayout'
import ProductUnitCard from 'src/components/ProductUnitCard'

const ManagerUnit = () => {
  const [visible, setVisible] = useState(false)
  const [name, setname] = useState("")
  const [quantity, setquantity] = useState("")
  const [product, setproduct] = useState("")
  const [products, setproducts] = useState("")
  const [sellPrice, setsellPrice] = useState("")
  const [units, setunits] = useState(null)

  const handleUnitAdd = async()=>{
    let formData = toFormData({name, quantity, productId: product, sellPrice})
    let response = await addUnit(formData)
    if(response.status == 200){
      response = await response.json()
      setunits([response.data, ...units])
      setVisible(false)
    }
  }

  const deleteItem = async(id)=>{
    let response = await deleteUnit(id)
    if(response.status == 200){
      setunits(units.filter((item) => item.id != id))
    }
  }

  useEffect(()=>{
    const fetchData = async()=>{
      let units = await getUnits()
      units = await units.json()
      setunits(units.data)
      console.log(units);

      let products = await getProducts()
      products = await products.json()
      setproducts(products.data)
    }
    fetchData()
  }, [])


  return (
    <ManagerLayout>
      <CCard>
        <CCardHeader><strong>Liste des unités</strong></CCardHeader>
        <CCardBody>
          <CButton onClick={() => setVisible(true)}>Ajouter une unité</CButton>
          <CRow className='mt-2'>
            {units && units.map((productUnit) => {
              if(productUnit.units.length > 0){
                return <CCol md={3}>
                  <ProductUnitCard productUnit={productUnit} />
                </CCol>
              }
            })}
          </CRow>
        </CCardBody>
      </CCard>
      <CModal visible={visible} onClose={() => setVisible(false)}>
        <CModalHeader>
          <CModalTitle>Ajouter une unité</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CFormLabel>Nom de l'unité</CFormLabel>
          <CFormInput type="text" value={name} onChange={(e)=>{setname(e.target.value)}}  />
          <CFormLabel>Quantité</CFormLabel>
          <CFormInput type="number" value={quantity} onChange={(e)=>{setquantity(e.target.value)}}  />
          <CFormLabel>Prix de vente</CFormLabel>
          <CFormInput type="number" value={sellPrice} onChange={(e)=>{setsellPrice(e.target.value)}}  />
          <CFormLabel>Produit</CFormLabel>
          <CFormSelect onChange={(e)=>{setproduct(e.target.value)}} >
            <option>Choisir un produit</option>
            {products && products.map((product, i) => <option key={i} value={product.id}>{product.name}</option>)}
          </CFormSelect>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={()=>{handleUnitAdd()}}>Enregistrer</CButton>
          <CButton color="secondary" onClick={() => setVisible(false)}>
            Fermer
          </CButton>
        </CModalFooter>
      </CModal>
    </ManagerLayout>
  )
}

export default ManagerUnit
