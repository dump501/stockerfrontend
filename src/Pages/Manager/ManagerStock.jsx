import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CTableRow,
  CTable,
  CTableHeaderCell,
  CTableDataCell,
  CTableHead,
  CTableBody,
  CButtonGroup,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody
} from '@coreui/react'
import { Link } from 'react-router-dom'
import { deleteProduct, deleteStock, getProducts, getStocks } from 'src/Api/Api'
import ManagerLayout from './ManagerLayout'
import { formatDate } from 'src/Helper/Helper'
import SpinnerPage from 'src/components/SpinnerPage'

const ManagerStock = () => {
  const [loading, setLoading] = useState(false)
  const [stocks, setstocks] = useState(null)

  const deleteItem = async (id) => {
    await deleteStock(id)
  }

  useEffect(() => {
    setLoading(true)
    const fetchData = async () => {
      let response = await getStocks()
      response = await response.json()
      setstocks(response.data)
      console.log(response);
    }
    fetchData()
    setLoading(false)

  }, [])
  return (
    <ManagerLayout>
    {loading ? <SpinnerPage /> :<CCard className="mb-4">
      <CCardHeader>
        <strong>Liste Gestionnaire</strong>
      </CCardHeader>
      <CCardBody>
        <CButton component={Link} to='/manager/stock/create'>Ajouter un stock</CButton>
        <CTable>
          <CTableHead>
            <CTableRow>
              <CTableHeaderCell scope="col">Produits</CTableHeaderCell>
              <CTableHeaderCell scope="col">Quantité</CTableHeaderCell>
              <CTableHeaderCell scope="col">Date de peremption</CTableHeaderCell>
              <CTableHeaderCell scope="col">Date d'arrivé en stock</CTableHeaderCell>
              <CTableHeaderCell scope="col">Date de production</CTableHeaderCell>
              <CTableHeaderCell scope="col">Actions</CTableHeaderCell>
            </CTableRow>
          </CTableHead>
          <CTableBody>
            {stocks && stocks.map((stock, i) => { return( <CTableRow key={i}>
              <CTableDataCell>{stock.product.name}</CTableDataCell>
              <CTableDataCell>{stock.quantity}</CTableDataCell>
              <CTableDataCell>{formatDate(stock.peremption_date)}</CTableDataCell>
              <CTableDataCell>{formatDate(stock.created_at)}</CTableDataCell>
              <CTableDataCell>{formatDate(stock.production_date)}</CTableDataCell>
              <CTableDataCell>
                <CButtonGroup>
                  {/* <CButton href="#" color="primary">
                    Editer
                  </CButton> */}
                </CButtonGroup>
              </CTableDataCell>
            </CTableRow>)})}
          </CTableBody>
        </CTable>
      </CCardBody>
    </CCard>}
    </ManagerLayout>
  )
}

export default ManagerStock
