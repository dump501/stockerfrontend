import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { CNavItem, CSidebar, CSidebarBrand, CSidebarNav, CSidebarToggler } from '@coreui/react'
import CIcon from '@coreui/icons-react'


import { logoNegative } from 'src/assets/brand/logo-negative'
import { sygnet } from 'src/assets/brand/sygnet'

import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'
import { AppSidebarNav } from 'src/components/AppSidebarNav'
import { cilAccountLogout, cilSpeedometer } from '@coreui/icons'
import { AppFooter, AppHeader } from 'src/components'

const navigation = [
  {
    component: CNavItem,
    name: 'Tableau de bord',
    to: '/manager/dashboard',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />
  },
  {
    component: CNavItem,
    name: 'Produits',
    to: '/manager/product',
    icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />
  },
  {
    component: CNavItem,
    name: 'Stocks',
    to: '/manager/stock',
    icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />
  },
  {
    component: CNavItem,
    name: 'Unité de produit',
    to: '/manager/unit',
    icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />
  },
  {
    component: CNavItem,
    name: 'Rayons',
    to: '/manager/rayon',
    icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />
  },
  {
    component: CNavItem,
    name: 'Notifications',
    to: '/manager/rule',
    icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />
  },
  {
    component: CNavItem,
    name: 'Logout',
    to: '/logout',
    icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />
  },
]

const ManagerLayout = ({children}) => {
  const dispatch = useDispatch()
  const unfoldable = useSelector((state) => state.sidebarUnfoldable)
  const sidebarShow = useSelector((state) => state.sidebarShow)
  return (
    <div>

      <CSidebar
        position="fixed"
        unfoldable={unfoldable}
        visible={sidebarShow}
        onVisibleChange={(visible) => {
          dispatch({ type: 'set', sidebarShow: visible })
        }}
      >
        <CSidebarBrand className="d-none d-md-flex" to="/">
        <h1>Stoker</h1>
          <CIcon className="sidebar-brand-narrow" icon={sygnet} height={35} />
        </CSidebarBrand>
        <CSidebarNav>
          <SimpleBar>
            <AppSidebarNav items={navigation} />
          </SimpleBar>
        </CSidebarNav>
        <CSidebarToggler
          className="d-none d-lg-flex"
          onClick={() => dispatch({ type: 'set', sidebarUnfoldable: !unfoldable })}
        />
      </CSidebar>
      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <AppHeader />
        <div className="body flex-grow-1 px-3">
          {children}
        </div>
        <AppFooter />
      </div>
    </div> 
  )
}

export default ManagerLayout
