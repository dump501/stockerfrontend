import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CForm,
  CFormInput,
  CFormLabel,
  CFormSelect,
  CCol,
} from '@coreui/react'
import { useForm } from 'react-hook-form';
import { toFormData } from 'src/Helper/Helper';
import { addStock, getProducts } from 'src/Api/Api';
import { useNavigate } from 'react-router-dom';
import ManagerLayout from './ManagerLayout'
import SpanError from 'src/components/SpanError';
import SpinnerPage from 'src/components/SpinnerPage';

const ManagerStockAdd = () => {
  const [loading, setLoading] = useState(false)
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [products, setproducts] = useState(null)
  let navigate = useNavigate()

  const handleForm = async (data) => {

    if (Object.keys(errors).length === 0) {
      setLoading(true)
      let formData = toFormData(data)
      formData.append("buyQuantity", data.quantity)
      let response = await addStock(formData)
      response = await response.json()
      console.log(response);
      setTimeout(() => {
        navigate("/manager/stock")
      }, 3000);
    } else {
      alert("Tous les champs doivent être rempli")
    }

  }
  useEffect(() => {
    setLoading(true)
    const fetchData = async () => {
      let response = await getProducts()
      response = await response.json()
      setproducts(response.data)
    }
    fetchData()
    setLoading(false)
  }, [])


  return (
    <ManagerLayout>
      {loading ? <SpinnerPage /> :<CCard className="mb-4">
        <CCardHeader>
          <strong>Ajouter un produit</strong>
        </CCardHeader>
        <CCardBody>
          <CForm className="row g-3" onSubmit={handleSubmit(handleForm)}>
            <CCol md={6}>
              <CFormLabel>Produit {errors.productId && <SpanError message="le produit est requis" />}</CFormLabel>
              <CFormSelect  {...register("productId", { required: true })} >

                <option>Selectionner le produit</option>
                {products && products.map((product, i) => <option key={i} value={product.id}>{product.name}</option>)}
              </CFormSelect>
            </CCol>
            <CCol md={6}>
              <CFormLabel>Quantite {errors.unitPrice && <SpanError />}</CFormLabel>
              <CFormInput type="number" {...register("quantity", { required: true })} />
            </CCol>
            <CCol md={6}>
              <CFormLabel>Prix d'achat {errors.buyPrice && <SpanError />}</CFormLabel>
              <CFormInput type="number" {...register("buyPrice", { required: true })} />
            </CCol>
            <CCol md={6}>
              <CFormLabel>Data de production {errors.productionDate && <SpanError />}</CFormLabel>
              <CFormInput type="datetime-local" {...register("productionDate", { required: true })} />
            </CCol>
            <CCol md={6}>
              <CFormLabel>Data de peremption {errors.peremptionDate && <SpanError />}</CFormLabel>
              <CFormInput type="datetime-local" {...register("peremptionDate", { required: true })} />
            </CCol>
            <CCol xs={12}>
              <CButton type="submit">Enregistrer</CButton>
            </CCol>
          </CForm>
        </CCardBody>
      </CCard>}
    </ManagerLayout>
  )
}

export default ManagerStockAdd
