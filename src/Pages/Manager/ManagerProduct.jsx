import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CTableRow,
  CTable,
  CTableHeaderCell,
  CTableDataCell,
  CTableHead,
  CTableBody,
  CButtonGroup,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody
} from '@coreui/react'
import { Link } from 'react-router-dom'
import { deleteProduct, getProducts } from 'src/Api/Api'
import ManagerLayout from './ManagerLayout'
import SpinnerPage from 'src/components/SpinnerPage'

const StockMoal = ({isVisible, setisVisible}) => {
  return (
    <>
      <CModal visible={isVisible} onClose={() => setisVisible(false)}>
        <CModalHeader>
          <CModalTitle>Details du stock</CModalTitle>
        </CModalHeader>
        <CModalBody>Woohoo, you&#39;re reading this text in a modal!</CModalBody>
      </CModal>
    </>
  )
}

const ManagerProduct = () => {
  const [loading, setLoading] = useState(false)
  const [products, setproducts] = useState(null)
  const [isVisible, setisVisible] = useState(false)

  const deleteItem = async (id) => {
    await deleteProduct(id)
  }

  useEffect(() => {
    setLoading(true)
    const fetchData = async () => {
      let response = await getProducts()
      response = await response.json()
      setproducts(response.data)
      console.log(response);
    }
    fetchData()
    setLoading(false)

  }, [])
  return (
    <ManagerLayout>
    {loading ? <SpinnerPage /> :<CCard className="mb-4">
      <CCardHeader>
        <strong>Liste Gestionnaire</strong>
      </CCardHeader>
      <CCardBody>
        <CButton component={Link} to='/manager/product/create'>Ajouter un produit</CButton>
        <CTable>
          <CTableHead>
            <CTableRow>
              <CTableHeaderCell scope="col">Nom</CTableHeaderCell>
              <CTableHeaderCell scope="col">Prix unitaire</CTableHeaderCell>
              <CTableHeaderCell scope="col">Rayon</CTableHeaderCell>
              <CTableHeaderCell scope="col">Actions</CTableHeaderCell>
            </CTableRow>
          </CTableHead>
          <CTableBody>
            {products && products.map((product, i) => { let date = new Date(product.peremption_date); return( <CTableRow key={i}>
              <CTableDataCell>{product.name}</CTableDataCell>
              <CTableDataCell>{product.unit_price}</CTableDataCell>
              <CTableDataCell>{product.rayon.name}</CTableDataCell>
              <CTableDataCell>
                <CButtonGroup>
                  <CButton href="#" color="warning">
                    Editer
                  </CButton>
                  <CButton color="danger" onClick={(e)=>{deleteItem(product.id)}}>
                    Suppr.
                  </CButton>
                </CButtonGroup>
              </CTableDataCell>
            </CTableRow>)})}
          </CTableBody>
        </CTable>
      </CCardBody>
    </CCard>}
    </ManagerLayout>
  )
}

export default ManagerProduct
