import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CTableRow,
  CTable,
  CTableHeaderCell,
  CTableDataCell,
  CTableHead,
  CTableBody,
  CButtonGroup
} from '@coreui/react'
import AdminLayout from './AdminLayout'
import { Link } from 'react-router-dom'
import { deleteProduct, getProducts } from 'src/Api/Api'

const Product = () => {
  const [loading, setLoading] = useState(false)
  const [products, setproducts] = useState(null)

  const deleteItem = async (id) => {
    await deleteProduct(id)
  }

  useEffect(() => {
    const fetchData = async () => {
      let response = await getProducts()
      response = await response.json()
      setproducts(response.data)
      console.log(response);
    }
    fetchData()

  }, [])
  return (
    <AdminLayout>
    <CCard className="mb-4">
      <CCardHeader>
        <strong>Liste Gestionnaire</strong>
      </CCardHeader>
      <CCardBody>
        <CButton component={Link} to='/admin/product/create'>Ajouter un produit</CButton>
        <CTable>
          <CTableHead>
            <CTableRow>
              <CTableHeaderCell scope="col">Nom</CTableHeaderCell>
              <CTableHeaderCell scope="col">Quantité</CTableHeaderCell>
              <CTableHeaderCell scope="col">Prix unitaire</CTableHeaderCell>
              <CTableHeaderCell scope="col">Date péremption</CTableHeaderCell>
              <CTableHeaderCell scope="col">Rayon</CTableHeaderCell>
              <CTableHeaderCell scope="col">Actions</CTableHeaderCell>
            </CTableRow>
          </CTableHead>
          <CTableBody>
            {products && products.map((product, i) => { let date = new Date(product.peremption_date); return( <CTableRow key={i}>
              <CTableDataCell>{product.name}</CTableDataCell>
              <CTableDataCell>{product.quantity}</CTableDataCell>
              <CTableDataCell>{product.unit_price}</CTableDataCell>
              <CTableDataCell>{`${date.getDate()} / ${date.getMonth()} / ${date.getFullYear()} à ${date.getHours()} : ${date.getMinutes()}`}</CTableDataCell>
              <CTableDataCell>{product.rayon.name}</CTableDataCell>
              <CTableDataCell>
                <CButtonGroup>
                  {/* <CButton href="#" color="warning" active>
                    Editer
                  </CButton> */}
                  <CButton color="danger" onClick={(e)=>{deleteItem(product.id)}}>
                    Supprimmer
                  </CButton>
                </CButtonGroup>
              </CTableDataCell>
            </CTableRow>)})}
          </CTableBody>
        </CTable>
      </CCardBody>
    </CCard>
    </AdminLayout>
  )
}

export default Product
