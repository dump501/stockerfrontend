import React, { useState } from 'react'
import AdminSidebar from './AdminSidebar'
import { AppFooter, AppHeader } from 'src/components'
import SpinnerPage from 'src/components/SpinnerPage'
import { LoadingProvider } from 'src/Context/LoadingContext'

const AdminLayout = ({ children }) => {
  return (
    <div>
      <AdminSidebar />
      <LoadingProvider>
        <div className="wrapper d-flex flex-column min-vh-100 bg-light">
          <AppHeader />
          <div className="body flex-grow-1 px-3">
            {children}
          </div>
          <AppFooter />
        </div>
      </LoadingProvider>
    </div>
  )
}

export default AdminLayout
