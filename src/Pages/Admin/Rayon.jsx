import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CFormInput,
  CFormLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CTable,
  CTableHeaderCell,
  CTableDataCell,
  CTableHead,
  CTableBody,
  CTableRow,
  CButtonGroup
} from '@coreui/react'
import AdminLayout from './AdminLayout'
import { toFormData } from 'src/Helper/Helper'
import { addRayon, deleteRayon, getRayons } from 'src/Api/Api'


const Rayon = () => {
  const [loading, setLoading] = useState(true)
  const [visible, setVisible] = useState(false)
  const [name, setname] = useState("")
  const [rayons, setrayons] = useState(null)

  const handleRayonAdd = async()=>{
    let formData = toFormData({name})
    let response = await addRayon(formData)
    if(response.status == 200){
      response = await response.json()
      setrayons([response.data, ...rayons])
    }
  }

  const deleteItem = async(id)=>{
    let response = await deleteRayon(id)
    if(response.status == 200){
      setrayons(rayons.filter((item) => item.id != id))
    }
  }

  useEffect(()=>{
    const fetchData = async()=>{
      let rayons = await getRayons()
      rayons = await rayons.json()
      setrayons(rayons.data)
    }
    fetchData()
  }, [])

  return (
    <AdminLayout>
      <CCard>
        <CCardHeader>Liste des rayons</CCardHeader>
        <CCardBody>
          <CButton onClick={() => setVisible(true)}>Ajouter un Rayon</CButton>
          <CTable>
            <CTableHead>
              <CTableRow>
                <CTableHeaderCell scope="col">Id</CTableHeaderCell>
                <CTableHeaderCell scope="col">Nom</CTableHeaderCell>
                <CTableHeaderCell scope="col">Actions</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {rayons && rayons.map((rayon, i) => (<CTableRow key={i}>
                <CTableDataCell>{i+1}</CTableDataCell>
                <CTableDataCell>{rayon.name}</CTableDataCell>
                <CTableDataCell>
                  <CButtonGroup>
                    {/* <CButton href="#" color="warning" active>
                    Editer
                  </CButton> */}
                    <CButton color="danger" onClick={(e) => { deleteItem(rayon.id) }}>
                      Supprimmer
                    </CButton>
                  </CButtonGroup>
                </CTableDataCell>
              </CTableRow>))}
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
      <CModal visible={visible} onClose={() => setVisible(false)}>
        <CModalHeader>
          <CModalTitle>Ajouter un Rayon</CModalTitle>
        </CModalHeader>
        <CModalBody>
        <CFormLabel>Nom du rayon</CFormLabel>
          <CFormInput type="text" value={name} onChange={(e)=>{setname(e.target.value)}}  />
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={()=>{handleRayonAdd()}}>Enregistrer</CButton>
          <CButton color="secondary" onClick={() => setVisible(false)}>
            Fermer
          </CButton>
        </CModalFooter>
      </CModal>
    </AdminLayout>
  )
}

export default Rayon
