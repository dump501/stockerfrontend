import React, { useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CForm,
  CFormInput,
  CFormLabel,
  CFormSelect,
  CCol,
} from '@coreui/react'
import AdminLayout from './AdminLayout'
import { useForm } from 'react-hook-form';
import { toFormData } from 'src/Helper/Helper';
import { addUser } from 'src/Api/Api';
import { useNavigate } from 'react-router-dom';
import SpinnerPage from 'src/components/SpinnerPage';

const SpanError = () => {
  return <span style={{ color: "red" }}>Ce champ est requis</span>
}

const ManagerAdd = () => {
  const [loading, setLoading] = useState(false)
  const { register, handleSubmit, formState: { errors } } = useForm();
  let navigate = useNavigate()

  const handleForm = async (data) => {

    if (Object.keys(errors).length === 0) {
      setLoading(true)
      let formData = toFormData(data)
      formData.append("role_id", 2)
      let response = await addUser(formData)
      response = await response.json()
      setTimeout(() => {
        navigate("/admin/manager")
      }, 3000);
    } else {
      alert("Tous les champs doivent être rempli")
    }

  }
  return (
    <AdminLayout>
      {loading ? <SpinnerPage /> :<CCard className="mb-4">
        <CCardHeader>
          <strong>Ajouter un gestionnaire de stock</strong>
        </CCardHeader>
        <CCardBody>
          <CForm className="row g-3" onSubmit={handleSubmit(handleForm)}>
            <CCol md={6}>
              <CFormLabel>Nom {errors.name && <SpanError />}</CFormLabel>
              <CFormInput type="text" {...register("name", { required: true })} />
            </CCol>
            <CCol md={6}>
              <CFormLabel>Email {errors.email && <SpanError />}</CFormLabel>
              <CFormInput type="email" {...register("email", { required: true })} />
            </CCol>
            <CCol md={6}>
              <CFormLabel>Téléphone {errors.phone && <SpanError />}</CFormLabel>
              <CFormInput type="text" {...register("phone", { required: true })} />
            </CCol>
            <CCol md={6}>
              <CFormLabel>Civilité {errors.civility && <SpanError />}</CFormLabel>
              <CFormSelect  {...register("civility", { required: true })} >

                <option>Selectionner la civilité</option>
                <option value={"Monsieur"}>M.</option>
                <option value={"Madame"}>Mme</option>
                <option value={"Mademoiselle"}>Mme</option>
              </CFormSelect>
            </CCol>
            <CCol md={6}>
              <CFormLabel>Mot de passe temporaire {errors.password && <SpanError />}</CFormLabel>
              <CFormInput type="password" {...register("password", { required: true })} />
            </CCol>
            <CCol xs={12}>
              <CButton type="submit">Enregistrer</CButton>
            </CCol>
          </CForm>
        </CCardBody>
      </CCard>}
    </AdminLayout>
  )
}

export default ManagerAdd
