import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CTableRow,
  CForm,
  CFormInput,
  CFormLabel,
  CFormTextarea,
  CTable,
  CTableHeaderCell,
  CTableDataCell,
  CFormSelect,
  CTableHead,
  CTableBody,
  CButtonGroup
} from '@coreui/react'
import AdminLayout from './AdminLayout'
import { Link } from 'react-router-dom'
import { deleteManager, getManagers } from 'src/Api/Api'
import SpinnerPage from 'src/components/SpinnerPage'

const Manager = () => {
  const [loading, setLoading] = useState(false)
  const [managers, setmanagers] = useState(null)

  const deleteItem = async (id) => {
    await deleteCashiers(id)
  }

  useEffect(() => {
    setLoading(true)
    const fetchData = async () => {
      let response = await getManagers()
      response = await response.json()
      setmanagers(response.data)
    }
    fetchData()
    setLoading(false)

  }, [])
  return (
    <AdminLayout>
    {loading ? <SpinnerPage /> :<CCard className="mb-4">
      <CCardHeader>
        <strong>Liste Gestionnaire</strong>
      </CCardHeader>
      <CCardBody>
        <CButton component={Link} to='/admin/manager/create'>Ajouter un gestionnaire</CButton>
        <CTable>
          <CTableHead>
            <CTableRow>
              <CTableHeaderCell scope="col">Nom</CTableHeaderCell>
              <CTableHeaderCell scope="col">Email</CTableHeaderCell>
              <CTableHeaderCell scope="col">Téléphone</CTableHeaderCell>
              <CTableHeaderCell scope="col">Civilité</CTableHeaderCell>
              <CTableHeaderCell scope="col">Actions</CTableHeaderCell>
            </CTableRow>
          </CTableHead>
          <CTableBody>
            {managers && managers.map((cashier) => ( <CTableRow>
              <CTableDataCell>{cashier.name}</CTableDataCell>
              <CTableDataCell>{cashier.email}</CTableDataCell>
              <CTableDataCell>{cashier.phone}</CTableDataCell>
              <CTableDataCell>{cashier.civility}</CTableDataCell>
              <CTableDataCell>
                <CButtonGroup>
                  {/* <CButton href="#" color="warning" active>
                    Editer
                  </CButton> */}
                  <CButton color="danger" onClick={(e)=>{deleteItem(cashier.id)}}>
                    Supprimmer
                  </CButton>
                </CButtonGroup>
              </CTableDataCell>
            </CTableRow>))}
          </CTableBody>
        </CTable>
      </CCardBody>
    </CCard>}
    </AdminLayout>
  )
}

export default Manager
