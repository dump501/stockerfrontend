import React, { useContext, useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CFormInput,
  CFormLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CTable,
  CTableHeaderCell,
  CTableDataCell,
  CTableHead,
  CTableBody,
  CTableRow,
  CButtonGroup,
  CForm,
  CCol,
  CRow
} from '@coreui/react'
import AdminLayout from './AdminLayout'
import { toFormData } from 'src/Helper/Helper'
import { addRayon, addSetting, deleteRayon, getRayons, getSettings } from 'src/Api/Api'
import { useNavigate } from 'react-router-dom'
import { LoadingContext, useLoading } from 'src/Context/LoadingContext'
import SpinnerPage from 'src/components/SpinnerPage'
import loadingStore from 'src/Context/Loading.mobx'

const AdminSetting = () => {
  const [loading, setLoading] = useState(true)
  let navigate = useNavigate()
  const [compagnyName, setcompagnyName] = useState("kk")
  const [compagnyShortName, setcompagnyShortName] = useState("")
  const [compagnyLogo, setcompagnyLogo] = useState(null)
  const [compagnyEmail, setcompagnyEmail] = useState("")
  const [compagnyPhone, setcompagnyPhone] = useState("")
  const [compagnyAddress, setcompagnyAddress] = useState("")
  const [setting, setsetting] = useState(null)

  const handleSettingAdd = async () => {
    let formData = toFormData({ name })
    let response = await addSetting(formData)
  }

  const handleForm = async (data) => {

    if (Object.keys(errors).length === 0) {
      let formData = toFormData(data)
      formData.append("role_id", 3)
      let response = await addUser(formData)
      response = await response.json()
      setTimeout(() => {
        navigate("/admin/cashier")
      }, 3000);
    } else {
      alert("Tous les champs doivent être rempli")
    }

  }

  useEffect(() => {
    const fetchData = async () => {
      let settings = await getSettings()
      settings = await settings.json()
      settings = settings.data
      setsetting(settings)
      setcompagnyName(settings.compagny_name)
      setcompagnyShortName(settings.compagny_short_name)
      setcompagnyEmail(settings.compagny_email)
      setcompagnyAddress(settings.compagny_address)
      setcompagnyPhone(settings.compagny_phone)
      setLoading(false)
    }
    fetchData()
  }, [])
  return (
    <AdminLayout>
      {loading ? <SpinnerPage /> : <CCard>
        <CCardHeader><strong>Paramètre de l'application</strong></CCardHeader>
        <CCardBody>
          {setting && <CRow>
            <CCol md={6}>
              <h4>mettre à jour les information de la société</h4>
              <CForm className="row g-3">
                <CCol md={12}>
                  <CFormLabel>Nom court de la société</CFormLabel>
                  <CFormInput
                    value={compagnyShortName}
                    onChange={(e) => { setcompagnyShortName(e.target.value) }}
                    type="text"
                    placeholder='nom court de la société' />
                </CCol>
                <CCol md={12}>
                  <CFormLabel>Nom complet de la société</CFormLabel>
                  <CFormInput
                    value={compagnyName}
                    onChange={(e) => { setcompagnyName(e.target.value) }}
                    type="text"
                    placeholder='nom complet de la société' />
                </CCol>
                <CCol md={12}>
                  <CFormLabel>email de la société</CFormLabel>
                  <CFormInput
                    value={compagnyEmail}
                    onChange={(e) => { setcompagnyEmail(e.target.value) }}
                    type="text"
                    placeholder='email de la société' />
                </CCol>
                <CCol md={12}>
                  <CFormLabel>Adresse de la société</CFormLabel>
                  <CFormInput
                    value={compagnyAddress}
                    onChange={(e) => { setcompagnyAddress(e.target.value) }}
                    type="text"
                    placeholder='Adresse de la société' />
                </CCol>
                <CCol md={12}>
                  <CFormLabel>Téléphone de la société</CFormLabel>
                  <CFormInput
                    value={compagnyPhone}
                    onChange={(e) => { setcompagnyPhone(e.target.value) }}
                    type="text"
                    placeholder='Téléphone de la société' />
                </CCol>
                <CCol md={12}>
                  <CFormLabel>Logo de la société</CFormLabel>
                  <CFormInput
                    value={compagnyLogo}
                    onChange={(e) => { setcompagnyLogo(e.target.files[0]) }}
                    type="file"
                    placeholder='Téléphone de la société' />
                </CCol>
                <CCol xs={12}>
                  <CButton type="submit">Enregistrer</CButton>
                </CCol>
              </CForm>
            </CCol>
            <CCol md={6}>
              <h4>Informations actuelles de la société</h4>
              <p><strong>Nom court de la société:</strong> {compagnyShortName}</p>
              <p><strong>Nom de la société:</strong> {compagnyName}</p>
              <p><strong>Email de la société:</strong> {compagnyEmail}</p>
              <p><strong>Adresse de la société:</strong> {compagnyAddress}</p>
              <p><strong>Téléphone de la société:</strong> {compagnyPhone}</p>
              <p><img src={setting.compagny_logo} style={{height: 200, width: "auto"}} /></p>
            </CCol>
          </CRow>}
        </CCardBody>
      </CCard>}
    </AdminLayout>
  )
}

export default AdminSetting
