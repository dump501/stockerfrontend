import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { CNavItem, CSidebar, CSidebarBrand, CSidebarNav, CSidebarToggler } from '@coreui/react'
import CIcon from '@coreui/icons-react'


import { logoNegative } from 'src/assets/brand/logo-negative'
import { sygnet } from 'src/assets/brand/sygnet'

import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'
import { AppSidebarNav } from 'src/components/AppSidebarNav'
import { cilAccountLogout, cilSpeedometer } from '@coreui/icons'

const navigation = [
    {
      component: CNavItem,
      name: 'Tableau de bord',
      to: '/cashier/dashboard',
      icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />
    },
    {
      component: CNavItem,
      name: 'Creation Facture',
      to: '/cashier/bill/create',
      icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />
    },
    {
      component: CNavItem,
      name: 'Liste Factures',
      to: '/cashier/bill',
      icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />
    },
    {
      component: CNavItem,
      name: 'Paramètres',
      to: '/cashier/setting',
      icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />
    },
    {
      component: CNavItem,
      name: 'Logout',
      to: '/logout',
      icon: <CIcon icon={cilAccountLogout} customClassName="nav-icon" />
    },
]

const CashierSidebar = () => {
    const dispatch = useDispatch()
    const unfoldable = useSelector((state) => state.sidebarUnfoldable)
    const sidebarShow = useSelector((state) => state.sidebarShow)
  return (
    <CSidebar
      position="fixed"
      unfoldable={unfoldable}
      visible={sidebarShow}
      onVisibleChange={(visible) => {
        dispatch({ type: 'set', sidebarShow: visible })
      }}
    >
      <CSidebarBrand className="d-none d-md-flex" to="/">
        <h1>Stoker</h1>
        <CIcon className="sidebar-brand-narrow" icon={sygnet} height={35} />
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          <AppSidebarNav items={navigation} />
        </SimpleBar>
      </CSidebarNav>
      <CSidebarToggler
        className="d-none d-lg-flex"
        onClick={() => dispatch({ type: 'set', sidebarUnfoldable: !unfoldable })}
      />
    </CSidebar>
  )
}

export default CashierSidebar
