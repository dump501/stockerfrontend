import React from 'react'
import { AppFooter, AppHeader } from 'src/components'
import CashierSidebar from './CashierSidebar'
import { CFormInput } from '@coreui/react'

const CashierLayout = ({children}) => {
  return (
    <div>
      <CashierSidebar />
      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <AppHeader />
        <div className="body flex-grow-1 px-3">
          {children}
        </div>
        <AppFooter />
      </div>
    </div>
  )
}

export default CashierLayout
