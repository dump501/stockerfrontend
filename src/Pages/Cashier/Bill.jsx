import React, { useEffect, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CTableRow,
  CForm,
  CFormInput,
  CFormLabel,
  CFormTextarea,
  CTable,
  CTableHeaderCell,
  CTableDataCell,
  CFormSelect,
  CTableHead,
  CTableBody,
  CRow,
  CCol
} from '@coreui/react'
import { DocsExample } from 'src/components'
import CashierLayout from './CashierLayout'
import { addBill, getProducts, getUnits } from 'src/Api/Api'
import { toFormData } from 'src/Helper/Helper'
import { toJS } from 'mobx'
import authStore from 'src/Context/Auth.mobx'
import { useNavigate } from 'react-router-dom'
import SpinnerPage from 'src/components/SpinnerPage'

// let products = [
//   {
//     id: 1,
//     name: "Patate",
//     unitPrice: 3450,
//   },
//   {
//     id: 2,
//     name: "Macabo",
//     unitPrice: 1250,
//   },
//   {
//     id: 3,
//     name: "Igname",
//     unitPrice: 5700,
//   },
//   {
//     id: 4,
//     name: "Mangue",
//     unitPrice: 1350,
//   },
//   {
//     name: "Ciment 32.5",
//     unitPrice: 5000,
//   },
// ]

const Bill = () => {
  const [loading, setLoading] = useState(false)
  const [billItems, setbillItems] = useState([])
  const [product, setproduct] = useState(null)
  const [products, setproducts] = useState(null)
  const [quantity, setquantity] = useState("")
  const [units, setunits] = useState(null)
  const [unit, setunit] = useState(null)
  const [currentUnits, setcurrentUnits] = useState(null)
  const [name, setname] = useState("Tsafack Djiogo Fritz Albin")
  const [phone, setphone] = useState("+237698406818")
  const [address, setaddress] = useState("PMUC")
  const [email, setemail] = useState("tsafack07albin@gmail")
  const [deliveryPhone, setdeliveryPhone] = useState("+237698406818")
  const [deliveryAddress, setdeliveryAddress] = useState("PMUC")
  const [civility, setcivility] = useState("Monsieur")
  const [total, settotal] = useState(0)
  const navigate = useNavigate()

  const computeTotal = (billItems) => {
    let computedTotal = 0;
    for (const item of billItems) {
      computedTotal += Number(Number(item.quantity) * Number(item.product.unit_price))
    }
    console.log("called" + computedTotal);
    return settotal(computedTotal);
  }

  const handleBillItemAdd = (e) => {
    let existingProduct = billItems.filter((item) => item.product.id == product);

    // check the product quantity
    let selectedProduct = products.filter((item) => item.id == product)[0];
    if (selectedProduct.quantity < quantity) {
      alert(`Veillez diminuer la quantité du produit: ${selectedProduct.name} la quantité\
      en stock est: ${selectedProduct.quantity}`)
      return
    }

    // verify the product doesn't already exists and add
    if (existingProduct.length === 0) {
      setbillItems([...billItems, {
        product: products.filter((item) => item.id == product)[0],
        quantity
      }])
      console.log(billItems);
      computeTotal([...billItems, {
        product: products.filter((item) => item.id == product)[0],
        quantity
      }])

    } else {
      let currentBillItems = billItems.slice()

      for (let item of currentBillItems) {
        console.log(item);
        if (item.product.id == product) {
          item.quantity = quantity
          break;
        }
      }

      setbillItems(currentBillItems)

      computeTotal(currentBillItems)
    }
  }

  const handleBillAdd = async (e) => {
    if (name && address && phone && civility && email && deliveryAddress && deliveryPhone) {
      setLoading(true)

      let data = {
        name,
        address,
        phone,
        civility,
        email,
        deliveryAddress,
        deliveryPhone,
        user_id: toJS(authStore.user).id
      }
      console.log(data);
      let formData = toFormData(data)
      formData.append("billLignes", JSON.stringify(billItems))
      let response = await addBill(formData)
      setTimeout(() => {
        navigate("/cashier/bill")
      }, 3000);
    } else {
      alert("Veillez remplir tous les champs")
    }

  }

  const handleProductOnChange = (e) => {
    console.log("kk");
    setproduct(Number(e.target.value))
    let product = products.filter((item) => item.id == e.target.value)[0]
    console.log(product);
    setcurrentUnits(product.units ? product.units : []);
  }
  useEffect(() => {
    setLoading(true)
    const fetchData = async () => {
      let products = await getProducts()
      products = await products.json()
      setproducts(products.data)

      let units = await getUnits()
      units = await units.json()
      setunits(units.data)
    }
    fetchData()
    setLoading(false)
  }, [])
  return (
    <CashierLayout>
      {loading ? <SpinnerPage /> : <CRow>
        <CCol md={5}>
          <CCard className="mb-4">
            <CCardHeader>
              <strong>Ajout des produits</strong>
            </CCardHeader>
            <CCardBody>

              <CFormSelect onChange={(e) => { handleProductOnChange(e) }}>
                <option>Selectionner le produit</option>
                {products && products.map((product, i) => <option key={i} value={product.id}>{product.name} ({product.quantity} en stock)</option>)}
              </CFormSelect><br />
              <CFormInput
                type="number"
                placeholder="Quantite"
                value={quantity}
                onChange={(e) => { setquantity(e.target.value) }}
              />
              <CFormSelect onChange={(e) => { setproduct(Number(e.target.value)) }}>
                <option>Selectionner le l'unité</option>
                <option>unité par defaut</option>
                {currentUnits && currentUnits.map((unit, i) => <option key={i} value={unit.id}>{unit.name}</option>)}
              </CFormSelect>
              <br />
              <CButton color={"primary"} onClick={handleBillItemAdd}>
                Ajouter
              </CButton>
            </CCardBody>
          </CCard>
          <CCard className="mb-4">
            <CCardHeader>
              <strong>Informations du client</strong>
            </CCardHeader>
            <CCardBody>
              Nom complet du client
              <CFormInput
                type="text"
                placeholder="Nom complet"
                value={name}
                onChange={(e) => { setname(e.target.value) }}
              /><br />
              Téléphone du client
              <CFormInput
                type="text"
                placeholder="Téléphone du client"
                value={phone}
                onChange={(e) => { setphone(e.target.value) }}
              /><br />
              Email du client
              <CFormInput
                type="text"
                placeholder="Email du client"
                value={email}
                onChange={(e) => { setemail(e.target.value) }}
              /><br />
              Adresse du client
              <CFormInput
                type="text"
                placeholder="Adresse du client"
                value={address}
                onChange={(e) => { setaddress(e.target.value) }}
              /><br />
              civilité
              <CFormSelect onChange={e => { setcivility(e.target.value) }}>

                <option>Selectionner la civilité</option>
                <option value={"M."} selected>M.</option>
                <option value={"Mme"}>Mme</option>
              </CFormSelect><br />
              Adresse de livraison
              <CFormInput
                type="text"
                placeholder="Adresse de livraison"
                value={deliveryAddress}
                onChange={(e) => { setdeliveryAddress(e.target.value) }}
              /><br />
              Téléphone de livraison
              <CFormInput
                type="text"
                placeholder="Téléphone de livraison"
                value={deliveryPhone}
                onChange={(e) => { setdeliveryPhone(e.target.value) }}
              /><br />
            </CCardBody>
          </CCard>
        </CCol>
        <CCol md={7}>
          <CCard className="mb-4">
            {/* <CCardHeader>
              <strong>Facture</strong>
            </CCardHeader> */}
            <CCardBody>
              <u><h5>Information client: </h5></u>
              <p><strong>nom complet: </strong> {name}</p>
              <p><strong>numéro de téléphone: </strong> {phone}</p>
              <p><strong>Adresse: </strong> {address}</p><br />
              <u><h5>Contenu facture</h5></u>
              <CTable>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">Produit</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Prix unitaire</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Quantite</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Total</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {billItems && billItems.map((item, i) => (
                    <CTableRow key={i}>
                      <CTableDataCell>{item.product.name}</CTableDataCell>
                      <CTableDataCell>{item.product.unit_price} FCFA</CTableDataCell>
                      <CTableDataCell>{item.quantity}</CTableDataCell>
                      <CTableDataCell>{item.product.unit_price * item.quantity} FCFA</CTableDataCell>
                    </CTableRow>
                  ))}
                </CTableBody>
              </CTable><br />
              <u><h5>Total : {total} FCFA </h5></u>
              <CButton color={"primary"} onClick={(e) => { handleBillAdd(e) }}>
                Enregister & imprimer
              </CButton>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>}
    </CashierLayout>
  )
}

export default Bill
