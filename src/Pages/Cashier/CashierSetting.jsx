import React from 'react'
import { CCard, CCardBody, CCardHeader, CCol, CForm, CFormInput, CFormLabel, CSpinner } from '@coreui/react'
import { useForm } from 'react-hook-form';
import SpanError from 'src/components/SpanError';
import CashierLayout from './CashierLayout';
import { changePassword } from 'src/Api/Api';

const CashierSetting = () => {
  const { register, handleSubmit, formState: { errors } } = useForm();

  const handleForm = async (data) => {

    if (Object.keys(errors).length === 0) {
      let formData = toFormData(data)
      let response = await changePassword(formData)
      response = await response.json()
      console.log(response);
      setTimeout(() => {
        navigate("/cashier/product")
      }, 3000);
    } else {
      alert("Tous les champs doivent être rempli")
    }

  }


  return (
    <CashierLayout>
      <CCard>
        <CCardHeader>
          <strong>Sécurité</strong>
        </CCardHeader>
        <CCardBody>
          <CForm className="row g-3" onSubmit={handleSubmit(handleForm)}>
            <CCol md={12}>
              <CFormLabel>Ancien mot de passe {errors.password && <SpanError />}</CFormLabel>
              <CFormInput
                type="password"
                {...register("password", { required: true })}
                placeholder='Ancien mot de passe' />
            </CCol>
            <CCol md={6}>
              <CFormLabel>Nouveau mot de passe {errors.newPassword && <SpanError />}</CFormLabel>
              <CFormInput
                type="password"
                {...register("newPassword", { required: true })}
                placeholder='nouveau mot de passe' />
            </CCol>
            <CCol md={6}>
              <CFormLabel>Confirmer le nouveau mot de passe {errors.newPasswordConfirm && <SpanError />}</CFormLabel>
              <CFormInput
                type="password"
                {...register("newPasswordConfirm", { required: true })}
                placeholder='confimer mot de passer' />
            </CCol>
          </CForm>
        </CCardBody>
      </CCard>
    </CashierLayout>
  )
}

export default CashierSetting
