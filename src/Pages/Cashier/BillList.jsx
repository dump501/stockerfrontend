import React, { useEffect, useState } from 'react'
import CashierLayout from './CashierLayout'
import { getCashierBills, getSettings } from 'src/Api/Api'
import { toJS } from 'mobx'
import authStore from 'src/Context/Auth.mobx'
import { CButton, CCard, CCardBody, CCol, CFormInput, CFormLabel, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CRow, CTable, CTableBody, CTableDataCell, CTableHead, CTableHeaderCell, CTableRow } from '@coreui/react'
import { Document, Image, PDFViewer, Page, StyleSheet, Text, View } from '@react-pdf/renderer'
import { formatDate } from 'src/Helper/Helper'
import SpinnerPage from 'src/components/SpinnerPage'
const primaryColor = "#0d6efd"
const BillList = () => {
  const [loading, setLoading] = useState(false)
  const [bills, setbills] = useState(null)
  const [settings, setsettings] = useState(null)
  const [visible, setVisible] = useState(false)
  const [bill, setbill] = useState(null)

  const handleBillDetail = (bill) => {
    setbill(bill)
    console.log(bill);
    setVisible(true)
  }

  const computeTotal = (bill) => {
    let total = 0;
    for (const item of bill.billLignes) {
      total += Number(Number(item.quantity) * Number(item.product.unit_price))
    }
    return total;
  }

  const style = StyleSheet.create({
    page: {
      padding: 30,
      fontSize: 12,
    },
    header: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center"
    },
    header_left: {
      display: "flex",
      flexDirection: "column"
    },
    header_right: {
      fontSize: 30,
      fontWeight: 900
    },
    client: {
      marginTop: 50,
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between"
    },
    client_item: {
      maxWidth: 250
    },
    client_header: {
      color: primaryColor,
      fontSize: 14,
      fontWeight: 900,
    },
    content: {
      marginTop: 50
    },
    table: {
      display: "table",
      width: "auto",
      borderStyle: "solid",
      borderWidth: 1,
      borderRightWidth: 0,
      borderBottomWidth: 0
    },
    tableRow: {
      margin: "auto",
      flexDirection: "row"
    },
    tableCol: {
      width: "25%",
      borderStyle: "solid",
      borderWidth: 1,
      borderLeftWidth: 0,
      borderTopWidth: 0
    },
    tableCell: {
      margin: "auto",
      marginTop: 5
    },
    total: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      marginTop: 10
    },
    inline: {
      display: "flex",
      flexDirection: "row"
    },
  })

  useEffect(() => {
    setLoading(true)
    const fetchData = async () => {
      let response = await getCashierBills(toJS(authStore.user).id)
      response = await response.json()
      setbills(response.data)
      setbill(response.data[0])

      let settings = await getSettings()
      settings = await settings.json()
      setsettings(settings.data)
    }
    fetchData()
    setLoading(false)
  }, [])
  return (
    <CashierLayout>
      {loading ? <SpinnerPage /> :<><CRow>
        <CCol>
          <CCard>
            <CCardBody>
              <CTable>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">Code</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Nom du client</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Numéros du client</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Actions</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {bills && bills.map((item, i) => (
                    <CTableRow key={i}>
                      <CTableDataCell>{item.code}</CTableDataCell>
                      <CTableDataCell>{item.client_name}</CTableDataCell>
                      <CTableDataCell>{item.client_phone}</CTableDataCell>
                      <CTableDataCell>
                        <CButton onClick={() => { handleBillDetail(item) }}>Details</CButton>
                      </CTableDataCell>
                    </CTableRow>
                  ))}
                </CTableBody>
              </CTable><br />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      {settings && <CModal fullscreen visible={visible} onClose={() => setVisible(false)}>
        <CModalBody style={{ padding: 0, overflowY: "hidden" }}>
          <PDFViewer style={{ height: "100%", width: "100%" }}>
            <Document>
              <Page style={style.page}>
                <View style={style.header}>
                  <View style={style.header_left}>
                    <Text style={{ fontSize: 20, color: primaryColor, fontWeight: 1000 }}>{settings.compagny_short_name}</Text>
                    <Text>{settings.compagny_name}</Text>
                    <Text>{settings.compagny_address}</Text>
                    <Text>{settings.compagny_email}</Text>
                    <Text>{settings.compagny_phone}</Text>
                  </View>
                  <Image source={settings.compagny_logo} style={{ width: 200 }} />
                </View>
                <View style={style.client}>
                  <View style={style.client_item}>
                    <Text style={style.client_header}>Facturé à</Text>
                    <Text>{bill.client_civility}{bill.client_name}</Text>
                    <Text>{bill.client_address}</Text>
                    <Text>{bill.client_email}</Text>
                    <Text>{bill.client_phone}</Text>
                  </View>
                  <View style={style.client_item}>
                    <Text style={style.client_header}>Infos liraison</Text>
                    <Text wrap break>Adresse: {bill.delivery_address}</Text>
                    <Text>Tel: {bill.client_phone}</Text>
                  </View>
                </View>
                <View style={style.content}>
                  <View style={style.table}>
                    <View style={[style.tableRow, { backgroundColor: primaryColor, color: "white" }]}>
                      <View style={style.tableCol}>
                        <Text style={style.tableCell}>Désignation</Text>
                      </View>
                      <View style={style.tableCol}>
                        <Text style={style.tableCell}>Quantité</Text>
                      </View>
                      <View style={style.tableCol}>
                        <Text style={style.tableCell}>Prix unitaire</Text>
                      </View>
                      <View style={style.tableCol}>
                        <Text style={style.tableCell}>Total</Text>
                      </View>
                    </View>
                    {bill.billLignes.map((item, i) => {

                      return <View key={i} style={style.tableRow}>
                        <View style={style.tableCol}>
                          <Text style={style.tableCell}>{item.product.name}</Text>
                        </View>
                        <View style={style.tableCol}>
                          <Text style={style.tableCell}>{item.quantity} </Text>
                        </View>
                        <View style={style.tableCol}>
                          <Text style={style.tableCell}>{item.product.unit_price}</Text>
                        </View>
                        <View style={style.tableCol}>
                          <Text style={style.tableCell}>{Number(item.quantity) * Number(item.product.unit_price)} FCFA</Text>
                        </View>
                      </View>
                    })}
                  </View>
                </View>
                <View style={style.total}>
                  <View>
                    <Text style={{maxWidth: 200}}>fait le {formatDate(bill.created_at)}, {settings.compagny_short_name}</Text>
                    <Text>caissier: {bill.cashier.name}</Text>
                  </View>
                  <View style={style.inline}>
                    <Text style={{
                      color: primaryColor,
                      fontSize: 14,
                      fontWeight: 700
                    }}>TOTAL A PAYER</Text>
                    <Text style={{
                      color: primaryColor,
                      fontSize: 14,
                      fontWeight: 700,
                      textDecoration: "underline",
                      marginLeft: 10
                    }}>{computeTotal(bill)} FCFA</Text>
                  </View>
                </View>
                
                <Text style={{textAlign: "center", marginTop: 20}}>Merci de faire affaire avec nous !!</Text>
              </Page>
            </Document>
          </PDFViewer>
        </CModalBody>
        <CModalFooter>
          <CButton color="danger" onClick={() => setVisible(false)}>
            Fermer
          </CButton>
        </CModalFooter>
      </CModal>}</>}
    </CashierLayout>
  )
}

export default BillList
