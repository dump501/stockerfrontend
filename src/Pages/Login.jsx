import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import { observer } from 'mobx-react-lite'
import { useForm } from 'react-hook-form'
import { toFormData } from 'src/Helper/Helper'
import { login } from 'src/Api/Api'
import authStore from 'src/Context/Auth.mobx'

const Login = observer(() => {
  let auth = authStore;
  const { register, handleSubmit, formState: {errors} } = useForm();
  const [email, setmail] = useState("user@test.com")
  const [password, setpassword] = useState("1234")
  const navigate = useNavigate()

  const handleLogin = async (data)=>{
    if(Object.keys(errors).length === 0){
      let formData = toFormData(data)
      let credentials = await login(formData)
      if(credentials.status == 200 /* && credentials.user && credentials.user.id */){
        credentials = await credentials.json()
        console.log(credentials);
        auth.setUser(credentials.user)
        auth.setUserToken(credentials.token.token)
        if(credentials.user.role_id === 1){
          navigate("/admin/dashboard")
        } else if(credentials.user.role_id === 2){
          navigate("/manager/dashboard")
        } else if(credentials.user.role_id === 3){
          navigate("/cashier/dashboard")
        } else {
          navigate("/")
        }
      } else {
        alert("mot de passe ou email incorrect")
      }
    }
  }
  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={handleSubmit(handleLogin)}>
                    <h1>Login</h1>
                    <p className="text-medium-emphasis">Sign In to your account</p>
                    {errors.email && <span>Ce champs est requis</span>} 
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput 
                        value={email} 
                        {...register("email", {required: "Le champ email est requis"})} 
                        placeholder="email" 
                        autoComplete="email"
                        onChange={(e)=>{setmail(e.target.value)}} />
                    </CInputGroup>
                    {errors.password && <span>Ce champ est requis</span>}
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        {...register("password", {required: "Le champ password est requis"})}
                        type="password"
                        placeholder="Password"
                        autoComplete="current-password"
                        value={password}
                        onChange={(e)=>{setpassword(e.target.value)}}
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                            <CButton color="primary" className="px-4" type='submit'>
                                Login
                            </CButton>
                      </CCol>
                      <CCol xs={6} className="text-right">
                        <CButton color="link" className="px-0">
                          Forgot password?
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-primary py-5" style={{ width: '44%' }}>
                <CCardBody className="text-center">
                  <div>
                    <h1>Sign up</h1>
                    <p>
                      Application de gestion flexible de stock avec notification en temps réel
                    </p>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
})

export default Login
