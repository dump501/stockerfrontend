
const apiRoot = "http://localhost:3333/api"

export async function login(formData) {
  try {
    let response = await fetch(`${apiRoot}/login`, {
      method: "POST",
      body: formData
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function addUser(formData) {
  try {
    let response = await fetch(`${apiRoot}/admin/user`, {
      method: "POST",
      body: formData
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function getCashiers() {
  try {
    let response = await fetch(`${apiRoot}/admin/cashier`)
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function deleteCashiers(id) {
  try {
    let response = await fetch(`${apiRoot}/admin/user/${id}`, {
      method: "DELETE"
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function getManagers() {
  try {
    let response = await fetch(`${apiRoot}/admin/manager`)
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function deleteManager(id) {
  try {
    let response = await fetch(`${apiRoot}/admin/user/${id}`, {
      method: "DELETE"
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function addProduct(formData) {
  try {
    let response = await fetch(`${apiRoot}/admin/product`, {
      method: "POST",
      body: formData
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function getProducts() {
  try {
    let response = await fetch(`${apiRoot}/admin/product`)
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function addSetting(formData) {
  try {
    let response = await fetch(`${apiRoot}/admin/setting`, {
      method: "POST",
      body: formData
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function getSettings() {
  try {
    let response = await fetch(`${apiRoot}/admin/setting`)
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function deleteProduct(id) {
  try {
    let response = await fetch(`${apiRoot}/admin/product/${id}`, {
      method: "DELETE"
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function addRayon(formData) {
  try {
    let response = await fetch(`${apiRoot}/admin/rayon`, {
      method: "POST",
      body: formData
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function getRayons() {
  try {
    let response = await fetch(`${apiRoot}/admin/rayon`)
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function deleteRayon(id) {
  try {
    let response = await fetch(`${apiRoot}/admin/rayon/${id}`, {
      method: "DELETE"
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function addBill(data) {
  try {
    let response = await fetch(`${apiRoot}/cashier/bill`, {
      method: "POST",
      body: data,
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function getCashierBills(id) {
  try {
    let response = await fetch(`${apiRoot}/cashier/${id}/bill`)
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function addStock(formData) {
  try {
    let response = await fetch(`${apiRoot}/stock-manager/stock`, {
      method: "POST",
      body: formData
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function getStocks() {
  try {
    let response = await fetch(`${apiRoot}/stock-manager/stock`)
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function deleteStock(id) {
  try {
    let response = await fetch(`${apiRoot}/stock-manager/stock/${id}`, {
      method: "DELETE"
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function addUnit(formData) {
  try {
    let response = await fetch(`${apiRoot}/stock-manager/unit`, {
      method: "POST",
      body: formData
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function getUnits() {
  try {
    let response = await fetch(`${apiRoot}/stock-manager/unit`)
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function deleteUnit(id) {
  try {
    let response = await fetch(`${apiRoot}/stock-manager/unit/${id}`, {
      method: "DELETE"
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function getRules() {
  try {
    let response = await fetch(`${apiRoot}/stock-manager/rule`)
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function addRule(formData) {
  try {
    let response = await fetch(`${apiRoot}/stock-manager/rule`, {
      method: "POST",
      body: formData
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function getUserRules(id) {
  try {
    let response = await fetch(`${apiRoot}/user/get-rule/${id}`)
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function addUserRule(formData) {
  try {
    let response = await fetch(`${apiRoot}/user/rule`, {
      method: "POST",
      body: formData
    })
    return response
  } catch (error) {
    console.log(error);
  }
}

export async function changePassword(formData) {
  try {
    let response = await fetch(`${apiRoot}/user/change-password`, {
      method: "POST",
      body: formData
    })
    return response
  } catch (error) {
    console.log(error);
  }
}