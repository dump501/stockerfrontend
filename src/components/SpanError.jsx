import React from 'react'

const SpanError = ({message}) => {
  return <span style={{ color: "red" }}>{message ? message : "Ce champ est requis"}</span>
}

export default SpanError
