import { CAccordion, CAccordionBody, CAccordionHeader, CAccordionItem, CCard, CCardBody } from '@coreui/react'
import React from 'react'
import { formatDate } from 'src/Helper/Helper'

const ProductUnitCard = ({productUnit}) => {
  return (
    <CCard>
      <CCardBody>
        <p><strong>{productUnit && productUnit.name}</strong></p>
        <CAccordion>
        {productUnit.units && productUnit.units.map((unit, i) => <CAccordionItem key={i}>
          <CAccordionHeader>{unit.name}</CAccordionHeader>
          <CAccordionBody>
            <div>Quantité: {unit.quantity}</div>
            <div>Prix: {unit.sell_price} FCFA</div>
          </CAccordionBody>
        </CAccordionItem> )}
        </CAccordion>
      </CCardBody>
    </CCard>
  )
}

export default ProductUnitCard
