import { CSpinner } from '@coreui/react'
import React from 'react'

const SpinnerPage = () => {
  return (
    <div style={{height: "81vh", width: "100%", display: "flex", alignItems: "center", justifyContent: "center"}}>
      <CSpinner color="danger" variant="grow" />
    </div>
  )
}

export default SpinnerPage
