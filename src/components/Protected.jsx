import { toJS } from 'mobx'
import { observer } from 'mobx-react'
import React from 'react'
import { Navigate } from 'react-router-dom'
import authStore from 'src/Context/Auth.mobx'
const Protected = observer(({ children, role }) => {
    const user = toJS(authStore.user)
    console.log("user", user);
    if (!user || !user.token || user.token === "") {
      return <Navigate to="/" replace />
    }
    if(role == 1 && user.role_id == 1){
      return children
    }
    if(role == 2 && user.role_id == 2){
      return children
    }
    if(role == 3 && user.role_id == 3){
      return children
    }
    return <Navigate to="/" replace />
})
export default Protected