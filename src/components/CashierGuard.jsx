import React from 'react'

const CashierGuard = observer(({ children }) => {
  const user = toJS(authStore.user)
  console.log("user", user);
  if (!user || !user.token || user.token === "" || user.id !== 3) {
    return <Navigate to="/" replace />
  }
  return children
})

export default CashierGuard
