import React, { Component, Suspense } from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import './scss/style.scss'
import Login from './Pages/Login'
import CashierDashboard from './Pages/Cashier/CashierDashboard'
import Bill from './Pages/Cashier/Bill'
import AdminDashboard from './Pages/Admin/AdminDashboard'
import Cashier from './Pages/Admin/Cashier'
import CashierAdd from './Pages/Admin/CashierAdd'
import Protected from './components/Protected'
import Manager from './Pages/Admin/Manager'
import ManagerAdd from './Pages/Admin/ManagerAdd'
import Product from './Pages/Admin/Product'
import ProductAdd from './Pages/Admin/ProductAdd'
import Rayon from './Pages/Admin/Rayon'
import Logout from './Pages/Logout'
import BillList from './Pages/Cashier/BillList'
import ManagerDashboard from './Pages/Manager/ManagerDashboard'
import ManagerProduct from './Pages/Manager/ManagerProduct'
import ManagerProductAdd from './Pages/Manager/ManagerProductAdd'
import ManagerRayon from './Pages/Manager/ManagerRayon'
import ManagerStock from './Pages/Manager/ManagerStock'
import ManagerStockAdd from './Pages/Manager/ManagerStockAdd'
import ManagerUnit from './Pages/Manager/ManagerUnit'
import ManagerRule from './Pages/Manager/ManagerRule'
import AdminSetting from './Pages/Admin/AdminSetting'
import CashierSetting from './Pages/Cashier/CashierSetting'
import ManagerSetting from './Pages/Manager/ManagerSetting'
import CustomRoutes from './components/CustomRoutes'

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const DefaultLayout = React.lazy(() => import('./layout/DefaultLayout'))

// Pages
// const Login = React.lazy(() => import('./views/pages/login/Login'))
// const Login = React.lazy(() => import('./Pages/Login'))
// const CashierDashboard = React.lazy(() => import('./Pages/Cashier/CashierDashboard'))
const Register = React.lazy(() => import('./views/pages/register/Register'))
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'))
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'))

class App extends Component {
  render() {
    return (
      <Router>
        <CustomRoutes>
              <Route exact path="/" name="Login Page" element={<Login />} />
              <Route exact path="/register" name="Register Page" element={<Register />} />
              <Route exact path="/404" name="Page 404" element={<Page404 />} />
              <Route exact path="/500" name="Page 500" element={<Page500 />} />
              <Route exact path="/logout" name="Login Page" element={<Logout />} />
              {/* <Route path="*" name="Home" element={<DefaultLayout />} /> */}
              {/* <Route path="/cashier">
              <Route path="dashboard" element={<CashierDashboard />} />
              <Route path="bill" element={<Bill />} />
            </Route> */}
              <Route path="/admin">
                <Route path="dashboard" element={<Protected role={1}><AdminDashboard /></Protected>} />
                <Route path="cashier" element={<Protected role={1}><Cashier /></Protected>} />
                <Route path="cashier/create" element={<Protected role={1}><CashierAdd /></Protected>} />
                <Route path="manager" element={<Protected role={1}><Manager /></Protected>} />
                <Route path="manager/create" element={<Protected role={1}><ManagerAdd /></Protected>} />
                <Route path="product" element={<Protected role={1}><Product /></Protected>} />
                <Route path="setting" element={<Protected role={1}><AdminSetting /></Protected>} />
                <Route path="product/create" element={<Protected role={1}><ProductAdd /></Protected>} />
                <Route path="rayon" element={<Protected role={1}><Rayon /></Protected>} />
              </Route>
              <Route path="/cashier">
                <Route path="dashboard" element={<Protected role={3}><CashierDashboard /></Protected>} />
                <Route path="bill/create" element={<Protected role={3}><Bill /></Protected>} />
                <Route path="bill" element={<Protected role={3}><BillList /></Protected>} />
                <Route path="setting" element={<Protected role={3}><CashierSetting /></Protected>} />
              </Route>
              <Route path="/manager">
                <Route path="dashboard" element={<Protected role={2}><ManagerDashboard /></Protected>} />
                <Route path="unit" element={<Protected role={2}><ManagerUnit /></Protected>} />
                <Route path="stock" element={<Protected role={2}><ManagerStock /></Protected>} />
                <Route path="stock/create" element={<Protected role={2}><ManagerStockAdd /></Protected>} />
                <Route path="product" element={<Protected role={2}><ManagerProduct /></Protected>} />
                <Route path="product/create" element={<Protected role={2}><ManagerProductAdd /></Protected>} />
                <Route path="rayon" element={<Protected role={2}><ManagerRayon /></Protected>} />
                <Route path="rule" element={<Protected role={2}><ManagerRule /></Protected>} />
                <Route path="setting" element={<Protected role={2}><ManagerSetting /></Protected>} />
              </Route>
        </CustomRoutes>
      </Router>
    )
  }
}

export default App
