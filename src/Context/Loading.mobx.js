import { makeAutoObservable } from "mobx";

class LoadingStore{
    isLoading = true

    constructor(){
        makeAutoObservable(this, {}, { autoBind: true })
    }

    setisLoading(val){
      this.isLoading = val
    }
}

let loadingStore = new LoadingStore()

export default loadingStore
