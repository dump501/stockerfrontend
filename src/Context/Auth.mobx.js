import { makeAutoObservable } from "mobx";

class AuthStore{
    user = null

    constructor(){
        let localUser = localStorage.getItem("user")
        if(localUser){
            this.user = JSON.parse(localUser)
        }
        makeAutoObservable(this, {}, { autoBind: true })
    }

    setUser(user){
        this.user = user
        localStorage.setItem("user", JSON.stringify(this.user))
    }

    setUserToken(token){
        this.user.token = token
        localStorage.setItem("user", JSON.stringify(this.user))
    }

    getUserToken(){
        return this.user.token
    }
}

let authStore = new AuthStore()

export default authStore
