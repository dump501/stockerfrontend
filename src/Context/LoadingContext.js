import { createContext, useContext, useState } from "react";

const LoadingContext = createContext({
  loading: true,
  setLoading: ()=>{}
});
// const LoadingDispatcherContext = createContext({
//   setLoa
// });

export function LoadingProvider({ children }) {
  const [loading, setLoading] = useState(true);
  const value = { loading, setLoading };
  useState(()=>{
    setLoading(true)
  }, [])
  return (
    <LoadingContext.Provider value={{loading: loading, setLoading: setLoading}}>{children}</LoadingContext.Provider>
  );
}

export function useLoading() {
  const context = useContext(LoadingContext);
  if (!context) {
    throw new Error("useLoading must be used within LoadingProvider");
  }
  return context;
}